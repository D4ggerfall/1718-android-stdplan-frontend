package at.spengergasse.finalprojectfrontend.retrofit;

import java.util.ArrayList;

import at.spengergasse.finalprojectfrontend.model.Stundenplan;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GetStundenplanService {

    @GET("/lessons/")
    Call<ArrayList<Stundenplan>> getStundenplanList();

    @GET("/lessons/teachers/{teacher}")
    Call<ArrayList<Stundenplan>> getStundenplanListByTeacher(@Path("teacher") String teacher);
}
