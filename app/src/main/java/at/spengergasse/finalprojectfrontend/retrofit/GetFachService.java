package at.spengergasse.finalprojectfrontend.retrofit;

import java.util.ArrayList;

import at.spengergasse.finalprojectfrontend.model.Fach;
import retrofit2.Call;
import retrofit2.http.GET;

public interface GetFachService {

    @GET("/subjects/")
    Call<ArrayList<Fach>> getallFacher();

}
