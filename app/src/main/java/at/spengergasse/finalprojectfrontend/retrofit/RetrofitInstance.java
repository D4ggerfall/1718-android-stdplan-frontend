package at.spengergasse.finalprojectfrontend.retrofit;

import android.util.Log;
import android.widget.Toast;

import java.net.Inet4Address;
import java.net.UnknownHostException;

import at.spengergasse.finalprojectfrontend.view.StuenplanListActivity;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {

    private static Retrofit retrofit;
    private static final String BASEURL_HOME = "http://192.168.0.136:8080/";
    private static final String BASEURL_EMULATOR = "http://10.0.2.2:8080/";
    private static final String BASEURL_SCHOOL = "http://10.91.76.145:8080/";
    public static Retrofit getInstance(){
        if (retrofit == null){
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASEURL_SCHOOL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }

}
