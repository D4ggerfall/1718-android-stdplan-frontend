package at.spengergasse.finalprojectfrontend.retrofit;

import java.util.ArrayList;

import at.spengergasse.finalprojectfrontend.model.Lehrer;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GetLehrerService {

    @GET("/teachers/")
    Call<ArrayList<Lehrer>> getTeacherList();

    @GET("/teachers/names/{lastname}")
    Call<ArrayList<Lehrer>> getLehrerByLastname(@Path("lastname") String lastname);

}
