package at.spengergasse.finalprojectfrontend.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import at.spengergasse.finalprojectfrontend.R;
import at.spengergasse.finalprojectfrontend.model.Fach;
import at.spengergasse.finalprojectfrontend.view.FachDetailActivity;
import at.spengergasse.finalprojectfrontend.view.FachDetailFragment;
import at.spengergasse.finalprojectfrontend.view.FachListActivity;

public class FachRecyclerViewAdapter extends RecyclerView.Adapter<FachViewHolder> {

    private FachListActivity mParentActivity;
    private List<Fach> mValues;
    private boolean mTwoPane;


    public FachRecyclerViewAdapter(FachListActivity parent, List<Fach> data, boolean twoPane){

        mParentActivity = parent;
        mValues = data;
        mTwoPane = twoPane;
    }

    @NonNull
    @Override
    public FachViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.fach_list_content, parent, false);
        return new FachViewHolder(view);
    }


    @Override
    public void onBindViewHolder(FachViewHolder holder, final int position) {
        holder.fachShortname.setText(mValues.get(position).getKurzname());
        holder.fachShortname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fach item = mValues.get(position);
                if (mTwoPane){
                    Bundle arguments = new Bundle();
                    arguments.putSerializable("item", item);
                    FachDetailFragment fragment = new FachDetailFragment();
                    fragment.setArguments(arguments);
                    mParentActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fach_detail_container, fragment)
                            .commit();
                }
                else {
                    try {
                        Context context = view.getContext();
                        Intent intent = new Intent(context, FachDetailActivity.class);
                        intent.putExtra("item", item);
                        context.startActivity(intent);
                    }catch (Exception e){
                        Log.e("AppError", e.getMessage());
                        Toast.makeText(mParentActivity,"error:" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
}
