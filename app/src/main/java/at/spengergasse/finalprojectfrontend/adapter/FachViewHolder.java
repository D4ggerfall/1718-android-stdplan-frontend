package at.spengergasse.finalprojectfrontend.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import at.spengergasse.finalprojectfrontend.R;

public class FachViewHolder extends RecyclerView.ViewHolder {

    TextView fachShortname;

    public FachViewHolder(View itemView) {
        super(itemView);
        fachShortname = itemView.findViewById(R.id.fach_main_shortname);
    }
}
