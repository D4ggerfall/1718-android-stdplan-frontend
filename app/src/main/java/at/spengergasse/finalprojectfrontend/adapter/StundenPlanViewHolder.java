package at.spengergasse.finalprojectfrontend.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import at.spengergasse.finalprojectfrontend.R;

public class StundenPlanViewHolder extends RecyclerView.ViewHolder {

    TextView klasse, fach, tag, stunde;

    public StundenPlanViewHolder (View itemView){
        super(itemView);
        klasse = itemView.findViewById(R.id.klasse);
        fach = itemView.findViewById(R.id.fach);
        tag =  itemView.findViewById(R.id.tag);
        stunde =  itemView.findViewById(R.id.stunde);
    }

}
