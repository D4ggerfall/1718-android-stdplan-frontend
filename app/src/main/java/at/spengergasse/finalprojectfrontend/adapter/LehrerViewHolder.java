package at.spengergasse.finalprojectfrontend.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import at.spengergasse.finalprojectfrontend.R;

public class LehrerViewHolder extends RecyclerView.ViewHolder {

    TextView lehrerShortName;

    public LehrerViewHolder(View itemView) {
        super(itemView);
        lehrerShortName = itemView.findViewById(R.id.lehrer_shortname_main);
    }
}
