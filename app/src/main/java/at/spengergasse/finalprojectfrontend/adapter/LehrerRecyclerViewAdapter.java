package at.spengergasse.finalprojectfrontend.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import at.spengergasse.finalprojectfrontend.R;
import at.spengergasse.finalprojectfrontend.model.Lehrer;
import at.spengergasse.finalprojectfrontend.view.LehrerDetailActivity;
import at.spengergasse.finalprojectfrontend.view.LehrerDetailFragment;
import at.spengergasse.finalprojectfrontend.view.LehrerListActivity;

public class LehrerRecyclerViewAdapter extends RecyclerView.Adapter<LehrerViewHolder> {

    private LehrerListActivity mParentActivity;
    private List<Lehrer> mValues;
    private boolean mTwoPane;

    public LehrerRecyclerViewAdapter(LehrerListActivity mParentActivity, List<Lehrer> mValues, boolean mTwoPane) {
        this.mParentActivity = mParentActivity;
        this.mValues = mValues;
        this.mTwoPane = mTwoPane;
    }

    @NonNull
    @Override
    public LehrerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.lehrer_list_content, parent, false);
        return new LehrerViewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull LehrerViewHolder holder, final int position) {

        holder.lehrerShortName.setText(mValues.get(position).getKurzname());
        holder.lehrerShortName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Lehrer item = mValues.get(position);
                if (mTwoPane){
                    Bundle arguments = new Bundle();
                    arguments.putSerializable("item", item);
                    LehrerDetailFragment fragment = new LehrerDetailFragment();
                    fragment.setArguments(arguments);
                    mParentActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.lehrer_detail_container, fragment)
                            .commit();
                }
                else {
                    try {
                        Context context = view.getContext();
                        Intent intent = new Intent(context, LehrerDetailActivity.class);
                        intent.putExtra("item", item);
                        context.startActivity(intent);
                    }catch (Exception e){
                        Log.e("AppError", e.getMessage());
                        Toast.makeText(mParentActivity,"error:" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
}
