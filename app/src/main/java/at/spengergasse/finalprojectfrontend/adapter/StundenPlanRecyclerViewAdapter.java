package at.spengergasse.finalprojectfrontend.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import at.spengergasse.finalprojectfrontend.R;
import at.spengergasse.finalprojectfrontend.model.Stundenplan;
import at.spengergasse.finalprojectfrontend.view.StuenplanDetailActivity;
import at.spengergasse.finalprojectfrontend.view.StuenplanDetailFragment;
import at.spengergasse.finalprojectfrontend.view.StuenplanListActivity;

public class StundenPlanRecyclerViewAdapter extends RecyclerView.Adapter<StundenPlanViewHolder> {

    private StuenplanListActivity mParentActivity;
    private List<Stundenplan> mValues;
    private boolean mTwoPane;

    public StundenPlanRecyclerViewAdapter(StuenplanListActivity parent, List<Stundenplan> data, boolean twoPane) {
        mValues = data;
        mParentActivity = parent;
        mTwoPane = twoPane;
    }


    @Override
    public final StundenPlanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
       View view = layoutInflater.inflate(R.layout.stuenplan_list_content, parent, false);
       return new StundenPlanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StundenPlanViewHolder holder, final int position) {
        holder.klasse.setText(mValues.get(position).getKlasse().getName());
        holder.fach.setText(mValues.get(position).getFach().getKurzname());
        String tag = mValues.get(position).getTag();
        switch (tag){
            case "1":
                holder.tag.setText("Montag");
                break;
            case "2":
                holder.tag.setText("Dienstag");
                break;
            case "3":
                holder.tag.setText("Mittwoch");
                break;
            case "4":
                holder.tag.setText("Donnerstag");
                break;
            case "5":
                holder.tag.setText("Freitag");
                break;
            case "6":
                holder.tag.setText("Samstag");
                break;
            case "7":
                holder.tag.setText("Der Entwickler ist dumm wenn du das siehst");
                break;
            default:
                holder.tag.setText("Der Entwickler ist dumm wenn du das siehst");
        }
        holder.stunde.setText(mValues.get(position).getStunde());
        holder.itemView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mTwoPane){
                            Bundle arguments = new Bundle();
                            arguments.putSerializable("item", mValues.get(position));
                            StuenplanDetailFragment fragment = new StuenplanDetailFragment();
                            fragment.setArguments(arguments);
                            mParentActivity.getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.stuenplan_detail_container, fragment)
                                    .commit();
                        }
                        else {
                            try {
                                Context context = view.getContext();
                                Intent intent = new Intent(context, StuenplanDetailActivity.class);
                                intent.putExtra("item", mValues.get(position));
                                context.startActivity(intent);
                            }catch (Exception e){
                                Log.e("AppError", e.getMessage());
                                Toast.makeText(mParentActivity,"error:" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
        );
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

}