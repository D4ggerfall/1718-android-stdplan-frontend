package at.spengergasse.finalprojectfrontend.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import at.spengergasse.finalprojectfrontend.R;
import at.spengergasse.finalprojectfrontend.adapter.StundenPlanRecyclerViewAdapter;
import at.spengergasse.finalprojectfrontend.model.Stundenplan;
import at.spengergasse.finalprojectfrontend.retrofit.GetStundenplanService;
import at.spengergasse.finalprojectfrontend.retrofit.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;

/**
 * An activity representing a list of Stundenplaene. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link StuenplanDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class StuenplanListActivity extends AppCompatActivity{

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private StundenPlanRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;
    private boolean mTwoPane;
    private ArrayList<Stundenplan> mValues;
    private EditText editTextLehrer;
    private ImageButton imgButtonSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stuenplan_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Stundenplan");
        setTitle("Stundenplan");

        editTextLehrer = findViewById(R.id.editTextLehrer);
        imgButtonSearch = findViewById(R.id.btLehrerFind);

        final GetStundenplanService service = RetrofitInstance.getInstance().create(GetStundenplanService.class);

        final retrofit2.Call<ArrayList<Stundenplan>> call = service.getStundenplanList();

        call.enqueue(new Callback<ArrayList<Stundenplan>>() {
            @Override
            public void onResponse(retrofit2.Call<ArrayList<Stundenplan>> call, Response<ArrayList<Stundenplan>> response) {
                mValues = response.body();
                generateStundenplanList(mValues);
            }

            @Override
            public void onFailure(retrofit2.Call<ArrayList<Stundenplan>> call, Throwable t) {
                Log.e("AppError", t.getMessage());
                Toast.makeText(StuenplanListActivity.this,"error" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        if (findViewById(R.id.stuenplan_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        View recyclerView = findViewById(R.id.stuenplan_list);
        assert recyclerView != null;

        imgButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextLehrer != null){
                    retrofit2.Call<ArrayList<Stundenplan>> apiCall2 = service.getStundenplanListByTeacher(editTextLehrer.getText().toString());
                    apiCall2.enqueue(new Callback<ArrayList<Stundenplan>>() {
                        @Override
                        public void onResponse(Call<ArrayList<Stundenplan>> call, Response<ArrayList<Stundenplan>> response) {
                            mValues = response.body();
                            generateStundenplanList(mValues);
                        }

                        @Override
                        public void onFailure(Call<ArrayList<Stundenplan>> call, Throwable t) {
                            Log.e("AppError", t.getMessage());
                            Toast.makeText(StuenplanListActivity.this,"error" + t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private void generateStundenplanList(ArrayList<Stundenplan> stundenplanList){
        recyclerView = (RecyclerView) findViewById(R.id.stuenplan_list);

        adapter = new StundenPlanRecyclerViewAdapter(this, stundenplanList, mTwoPane);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(StuenplanListActivity.this);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);
    }

}
