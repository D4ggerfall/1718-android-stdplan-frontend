package at.spengergasse.finalprojectfrontend.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;

import at.spengergasse.finalprojectfrontend.R;
import at.spengergasse.finalprojectfrontend.adapter.LehrerRecyclerViewAdapter;
import at.spengergasse.finalprojectfrontend.model.Lehrer;
import at.spengergasse.finalprojectfrontend.retrofit.GetLehrerService;
import at.spengergasse.finalprojectfrontend.retrofit.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * An activity representing a list of Lehrer. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link LehrerDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class LehrerListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private LehrerRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;
    private boolean mTwoPane;
    private ArrayList<Lehrer> mValues;
    private EditText editTextLehrerNachname;
    private ImageButton imgButtonSearchLeherNachname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lehrer_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Lehrer");
        setTitle("Lehrer");

        editTextLehrerNachname = findViewById(R.id.editTextLehrerNachname);
        imgButtonSearchLeherNachname = findViewById(R.id.btLehrerLastnameSearch);

        final GetLehrerService service = RetrofitInstance.getInstance().create(GetLehrerService.class);

        retrofit2.Call<ArrayList<Lehrer>> call = service.getTeacherList();

        call.enqueue(new Callback<ArrayList<Lehrer>>() {
            @Override
            public void onResponse(retrofit2.Call<ArrayList<Lehrer>> call, Response<ArrayList<Lehrer>> response) {
                mValues = response.body();
                generateFaecherList(mValues);
            }

            @Override
            public void onFailure(retrofit2.Call<ArrayList<Lehrer>> call, Throwable t) {
                Log.e("AppError", t.getMessage());
                Toast.makeText(LehrerListActivity.this,"error" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        if (findViewById(R.id.lehrer_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        View recyclerView = findViewById(R.id.lehrer_list);
        assert recyclerView != null;

        imgButtonSearchLeherNachname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextLehrerNachname.getText() != null){
                    retrofit2.Call<ArrayList<Lehrer>> apiCall2 = service.getLehrerByLastname(editTextLehrerNachname.getText().toString());
                    apiCall2.enqueue(new Callback<ArrayList<Lehrer>>() {
                        @Override
                        public void onResponse(Call<ArrayList<Lehrer>> call, Response<ArrayList<Lehrer>> response) {
                            mValues = response.body();
                            generateFaecherList(mValues);
                        }

                        @Override
                        public void onFailure(Call<ArrayList<Lehrer>> call, Throwable t) {
                            Log.e("AppError", t.getMessage());
                            Toast.makeText(LehrerListActivity.this,"error" + t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private void generateFaecherList(ArrayList<Lehrer> lehrerList){
        recyclerView = (RecyclerView) findViewById(R.id.lehrer_list);

        adapter = new LehrerRecyclerViewAdapter(this, lehrerList, mTwoPane);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(LehrerListActivity.this);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);
    }



}
