package at.spengergasse.finalprojectfrontend.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import at.spengergasse.finalprojectfrontend.R;
import at.spengergasse.finalprojectfrontend.adapter.FachRecyclerViewAdapter;
import at.spengergasse.finalprojectfrontend.model.Fach;
import at.spengergasse.finalprojectfrontend.retrofit.GetFachService;
import at.spengergasse.finalprojectfrontend.retrofit.RetrofitInstance;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * An activity representing a list of Faecher. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link FachDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class FachListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private FachRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;
    private boolean mTwoPane;
    private ArrayList<Fach> mValues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fach_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Fächer");
        setTitle("Fächer");

        GetFachService service = RetrofitInstance.getInstance().create(GetFachService.class);

        retrofit2.Call<ArrayList<Fach>> call = service.getallFacher();

        call.enqueue(new Callback<ArrayList<Fach>>() {
            @Override
            public void onResponse(retrofit2.Call<ArrayList<Fach>> call, Response<ArrayList<Fach>> response) {
                mValues = response.body();
                generateFaecherList(mValues);
            }

            @Override
            public void onFailure(retrofit2.Call<ArrayList<Fach>> call, Throwable t) {
                Log.e("AppError", t.getMessage());
                Toast.makeText(FachListActivity.this,"error" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        if (findViewById(R.id.stuenplan_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        View recyclerView = findViewById(R.id.fach_list);
        assert recyclerView != null;
    }

    private void generateFaecherList(ArrayList<Fach> fachList){
        recyclerView = (RecyclerView) findViewById(R.id.fach_list);

        adapter = new FachRecyclerViewAdapter(this, fachList, mTwoPane);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(FachListActivity.this);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);
    }


}
