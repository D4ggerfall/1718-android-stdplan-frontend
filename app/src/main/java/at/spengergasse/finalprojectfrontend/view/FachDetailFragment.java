package at.spengergasse.finalprojectfrontend.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import at.spengergasse.finalprojectfrontend.R;
import at.spengergasse.finalprojectfrontend.model.Fach;

/**
 * A fragment representing a single Fach detail screen.
 * This fragment is either contained in a {@link FachListActivity}
 * in two-pane mode (on tablets) or a {@link FachDetailActivity}
 * on handsets.
 */
public class FachDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private Fach mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FachDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            mItem = (Fach) getArguments().getSerializable("item");

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.getKurzname());
            }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fach_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.fach_guid)).setText(mItem.getGuid());
            ((TextView) rootView.findViewById(R.id.fach_kurzname)).setText(mItem.getKurzname());
            ((TextView) rootView.findViewById(R.id.fach_langname)).setText(mItem.getLangname());

        }

        return rootView;
    }
}
