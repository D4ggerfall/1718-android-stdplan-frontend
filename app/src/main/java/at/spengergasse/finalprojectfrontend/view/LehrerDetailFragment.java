package at.spengergasse.finalprojectfrontend.view;

import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import at.spengergasse.finalprojectfrontend.R;
import at.spengergasse.finalprojectfrontend.model.Lehrer;

/**
 * A fragment representing a single Lehrer detail screen.
 * This fragment is either contained in a {@link LehrerListActivity}
 * in two-pane mode (on tablets) or a {@link LehrerDetailActivity}
 * on handsets.
 */
public class LehrerDetailFragment extends Fragment {

    /**
     * The dummy content this fragment is presenting.
     */
    private Lehrer mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public LehrerDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mItem = (Lehrer) getArguments().getSerializable("item");

            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.getKurzname());
            }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.lehrer_detail, container, false);

        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.lehrer_guid)).setText(mItem.getGuid());
            ((TextView) rootView.findViewById(R.id.lehrer_vorname)).setText(mItem.getVorname());
            ((TextView) rootView.findViewById(R.id.lehrer_nachname)).setText(mItem.getNachname());
            ((TextView) rootView.findViewById(R.id.lehrer_kurzname)).setText(mItem.getKurzname());
            ((TextView) rootView.findViewById(R.id.lehrer_titel)).setText(mItem.getTitel());
            ((TextView) rootView.findViewById(R.id.lehrer_email)).setText(mItem.getEmail());

        }

        return rootView;
    }
}
