package at.spengergasse.finalprojectfrontend.view;

import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import at.spengergasse.finalprojectfrontend.R;
import at.spengergasse.finalprojectfrontend.model.Stundenplan;

/**
 * A fragment representing a single Stuenplan detail screen.
 * This fragment is either contained in a {@link StuenplanListActivity}
 * in two-pane mode (on tablets) or a {@link StuenplanDetailActivity}
 * on handsets.
 */
public class StuenplanDetailFragment extends Fragment {

    /**
     * The dummy content this fragment is presenting.
     */
    private Stundenplan mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public StuenplanDetailFragment() {
    }

    public Stundenplan getmItem() {
        return mItem;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mItem = (Stundenplan) getArguments().getSerializable("item");
            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout =  activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle("Stunde");
            }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.stuenplan_detail, container, false);

        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.guid)).setText(mItem.getGuid());
            ((TextView) rootView.findViewById(R.id.id_id)).setText(mItem.getId());
            ((TextView) rootView.findViewById(R.id.klasse)).setText(mItem.getKlasse().getName());
            ((TextView) rootView.findViewById(R.id.lehrer)).setText(mItem.getLehrer().getKurzname());
            ((TextView) rootView.findViewById(R.id.raum)).setText(mItem.getRaum());
            ((TextView) rootView.findViewById(R.id.tag)).setText(mItem.getTag());
            ((TextView) rootView.findViewById(R.id.fach)).setText(mItem.getFach().getKurzname());
            ((TextView) rootView.findViewById(R.id.stunde)).setText(mItem.getStunde());

        }

        return rootView;
    }

}
