package at.spengergasse.finalprojectfrontend.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Lehrer implements Serializable {


    private String guid;
    @SerializedName("shortName")
    private String kurzname;

    @SerializedName("lastName")
    private String nachname;
    @SerializedName("firstName")
    private String vorname;
    @SerializedName("title")
    private String titel;
    private String email;

    public Lehrer(String guid, String kurzname, String nachname, String vorname, String titel, String email) {
        this.guid = guid;
        this.kurzname = kurzname;
        this.nachname = nachname;
        this.vorname = vorname;
        this.titel = titel;
        this.email = email;
    }


    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getKurzname() {
        return kurzname;
    }

    public void setKurzname(String kurzname) {
        this.kurzname = kurzname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



}
