package at.spengergasse.finalprojectfrontend.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Fach implements Serializable {


    private String guid;
    @SerializedName("shortName")
    private String kurzname;
    @SerializedName("longName")
    private String langname;

    public Fach(String guid, String kurzname, String langname) {
        this.guid = guid;
        this.kurzname = kurzname;
        this.langname = langname;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getKurzname() {
        return kurzname;
    }

    public void setKurzname(String kurzname) {
        this.kurzname = kurzname;
    }

    public String getLangname() {
        return langname;
    }

    public void setLangname(String langname) {
        this.langname = langname;
    }
}
