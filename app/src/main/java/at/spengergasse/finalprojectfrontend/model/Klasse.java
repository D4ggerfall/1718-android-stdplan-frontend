package at.spengergasse.finalprojectfrontend.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;


public class Klasse implements Serializable{

    private UUID guid;
    private String name;
    @SerializedName("rootRoom")
    private String stammraum;
    private Lehrer kv;

    public Klasse(UUID guid, String name, String stammraum, Lehrer kv) {
        this.guid = guid;
        this.name = name;
        this.stammraum = stammraum;
        this.kv = kv;
    }

    public UUID getGuid() {
        return guid;
    }

    public void setGuid(UUID guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStammraum() {
        return stammraum;
    }

    public void setStammraum(String stammraum) {
        this.stammraum = stammraum;
    }

    public Lehrer getKv() {
        return kv;
    }

    public void setKv(Lehrer kv) {
        this.kv = kv;
    }
}
