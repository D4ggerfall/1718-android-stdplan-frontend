package at.spengergasse.finalprojectfrontend.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Stundenplan implements Serializable {

    private String guid;
    private String id;
    @SerializedName("currentClass")
    private Klasse klasse;
    @SerializedName("teacher")
    private Lehrer lehrer;
    @SerializedName("subject")
    private Fach fach;
    @SerializedName("room")
    private String raum;
    @SerializedName("day")
    private String tag;
    @SerializedName("hour")
    private String stunde;

    public Stundenplan(String guid, String id, Klasse klasse, Lehrer lehrer, Fach fach, String raum, String tag, String stunde) {
        this.guid = guid;
        this.id = id;
        this.klasse = klasse;
        this.lehrer = lehrer;
        this.fach = fach;
        this.raum = raum;
        this.tag = tag;
        this.stunde = stunde;
    }

    @Override
    public String toString() {
        return "Stundenplan{" +
                "guid='" + guid + '\'' +
                ", id='" + id + '\'' +
                ", klasse='" + klasse + '\'' +
                ", lehrer='" + lehrer + '\'' +
                ", fach='" + fach + '\'' +
                ", raum='" + raum + '\'' +
                ", tag='" + tag + '\'' +
                ", stunde='" + stunde + '\'' +
                '}';
    }

    public void setGuid(String guid) { this.guid = guid; }

    public void setId(String id) {
        this.id = id;
    }

    public void setKlasse(Klasse klasse) {
        this.klasse = klasse;
    }

    public void setLehrer(Lehrer lehrer) {
        this.lehrer = lehrer;
    }

    public void setFach(Fach fach) {
        this.fach = fach;
    }

    public void setRaum(String raum) {
        this.raum = raum;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setStunde(String stunde) {
        this.stunde = stunde;
    }

    public String getGuid() { return guid; }

    public String getId() {
        return id;
    }

    public Klasse getKlasse() {
        return klasse;
    }

    public Lehrer getLehrer() {
        return lehrer;
    }

    public Fach getFach() {
        return fach;
    }

    public String getRaum() {
        return raum;
    }

    public String getTag() {
        return tag;
    }

    public String getStunde() {
        return stunde;
    }
}
