package at.spengergasse.finalprojectfrontend;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import at.spengergasse.finalprojectfrontend.view.FachListActivity;
import at.spengergasse.finalprojectfrontend.view.LehrerListActivity;
import at.spengergasse.finalprojectfrontend.view.StuenplanListActivity;

public class MainActivity extends AppCompatActivity {

    private Button btStd, btLehr, btFaecher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btStd = findViewById(R.id.btStdplan);
        btLehr = findViewById(R.id.btLehrer);
        btFaecher = findViewById(R.id.btFaecher);

        btStd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, StuenplanListActivity.class);
                startActivity(intent);
            }
        });

        btLehr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LehrerListActivity.class);
                startActivity(intent);
            }
        });

        btFaecher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FachListActivity.class);
                startActivity(intent);
            }
        });

    }



}
